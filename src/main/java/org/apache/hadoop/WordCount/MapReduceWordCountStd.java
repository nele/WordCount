package org.apache.hadoop.WordCount;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class MapReduceWordCountStd extends Configured implements Tool {

	// map class
	public static class MapOperate extends
			Mapper<LongWritable, Text, Text, IntWritable> {
		public static final IntWritable inWritable = new IntWritable(1);
		public Text word = new Text();

		@Override
		public  void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			System.out.println(key+":"+value);
			String[] arr = value.toString().split(" ");
			for (String str : arr) {
				word.set(str);
			}
			context.write(word, inWritable);
		}

	}

	// reduce class
	public static class ReduceOperate extends
			Reducer<Text, IntWritable, Text, IntWritable> {
		
        private IntWritable outPutValue = new IntWritable();
		
        @Override
		public  void reduce(Text key, Iterable<IntWritable> values,
				Context context) throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			outPutValue.set(sum);
			context.write(key, outPutValue);
		}

	}

	// run method
	public int run(String[] args) throws Exception {
		Configuration conf = super.getConf();

		// create job
		Job job = Job.getInstance(conf, this.getClass().getSimpleName());
		job.setJarByClass(this.getClass());

		// set input path
		Path inPath = new Path(args[0]);
		FileInputFormat.addInputPath(job, inPath);

		// map
		job.setMapperClass(MapOperate.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);

		// conbile
		job.setCombinerClass(ReduceOperate.class);

		// reduce
		job.setReducerClass(ReduceOperate.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		// output
		Path outPath = new Path(args[1]);
		FileOutputFormat.setOutputPath(job, outPath);

		// submit
		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		args = new String[] {
				"hdfs://bigdata5:8020/user/nele/data/input/wc.txt",
				"hdfs://bigdata5:8020/user/nele/data/output/output3" };

		Configuration conf = new Configuration();

		int status = ToolRunner.run(conf, new MapReduceWordCountStd(), args);

		System.exit(status);
	}

}
