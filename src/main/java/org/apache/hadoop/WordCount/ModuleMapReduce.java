package org.apache.hadoop.WordCount;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/***
 * MapReduce Module
 * @author nele
 *
 */
public class ModuleMapReduce extends Configured implements Tool {

	// map class
	/**
	 * 
	 * @author nele
	 *  TODO
	 */
	public static class ModuleMapper extends
			Mapper<LongWritable, Text, LongWritable, Text> {

		@Override
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			// TODO
		}

	}

	// reduce class
	/***
	 * 
	 * @author nele
	 * TODO
	 */
	public static class ModuleReducer extends
			Reducer<LongWritable, Text, LongWritable, Text> {

		@Override
		//TODO
		public void reduce(LongWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			// TODO
		}

	}

	// run method
	public int run(String[] args) throws Exception {
		Configuration conf = super.getConf();

		// create job
		Job job = Job.getInstance(conf, this.getClass().getSimpleName());
		job.setJarByClass(this.getClass());

		// set input path
		Path inPath = new Path(args[0]);
		FileInputFormat.addInputPath(job, inPath);

		// map
		//TODO
		job.setMapperClass(ModuleMapper.class);
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(Text.class);
		
//########################shuffle###########################################
	   //1. partitioner 
	      //job.setPartitionerClass(cls);
	   // 2.sort
		  //job.setSortComparatorClass(cls);
	  // 3. conbine
		//job.setCombinerClass(ModuleReducer.class);
	  // 4.	compress
	      //set by configuraation 
	  //5.group 
	     //job.setGroupingComparatorClass(cls);
	 
//########################shuffle###########################################
		
		
		// reduce
		//TODO
		job.setReducerClass(ModuleReducer.class);
		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(Text.class);

		// output
		Path outPath = new Path(args[1]);
		FileOutputFormat.setOutputPath(job, outPath);

		// submit
		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
//		args = new String[] {
//				"hdfs://bigdata5:8020/user/nele/data/input/wc.txt",
//				"hdfs://bigdata5:8020/user/nele/data/output/output3" };

		Configuration conf = new Configuration();

		int status = ToolRunner.run(conf, new ModuleMapReduce(), args);

		System.exit(status);
	}

}
