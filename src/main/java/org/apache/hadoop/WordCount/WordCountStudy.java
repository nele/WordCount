package org.apache.hadoop.WordCount;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class WordCountStudy {

	public static class MapOperate extends
			Mapper<LongWritable, Text, Text, IntWritable> {

		private final static IntWritable intWritable = new IntWritable(1);
		private Text word = new Text();

		@Override
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			System.out.println(key + ":" + value);
			String[] words = value.toString().split(" ");
			for (String str : words) {
				word.set(str);
				context.write(word, intWritable);
			}
		}

	}

	// reduce
	public static class ReduceOperate extends
			Reducer<Text, IntWritable, Text, IntWritable> {
		public IntWritable result = new IntWritable();

		@Override
		public void reduce(Text key, Iterable<IntWritable> values,
				Context context) throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable value : values) {
				sum += value.get();
			}
			result.set(sum);
			context.write(key, result);
		}
	}

	public int run(String args[]) throws IOException, ClassNotFoundException,
			InterruptedException {

		String inputPath = args[0];
		String outputPath = args[1];

		// create conf
		Configuration conf = new Configuration();

		// create job
		Job job = Job.getInstance(conf, this.getClass().getSimpleName());

		System.out.println(this.getClass().getSimpleName());

		// set jar
		job.setJarByClass(this.getClass());

		/*************set input**********************/
		// file input
		FileInputFormat.addInputPath(job, new Path(inputPath));

		/************* set map *************/
		// set mapper class
		job.setMapperClass(MapOperate.class);
		// set map out
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);

		// set combine class
		job.setCombinerClass(ReduceOperate.class);

		/************** set reduce ********************/
		// set reduce
		job.setReducerClass(ReduceOperate.class);
		// set output key and value
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		/*************set output**********************/
		// set output
		FileOutputFormat.setOutputPath(job, new Path(outputPath));

		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws ClassNotFoundException,
			IOException, InterruptedException {

		args = new String[] {
				"hdfs://bigdata5:8020/user/nele/data/input/wc.txt",
				"hdfs://bigdata5:8020/user/nele/data/output/output1" };

		WordCountStudy wordCount = new WordCountStudy();
		int status = wordCount.run(args);

		System.exit(status);
	}
}
