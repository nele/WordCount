package org.apache.hadoop.mapreduce.app;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.io.MobileDataWritable;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/***
 * MapReduce Module
 * 
 * @author nele
 * 
 */
public class MobileDataMapReduce extends Configured implements Tool {

	// map class
	/**
	 * 
	 * @author nele
	 * 
	 */
	public static class MobileDataMapper extends
			Mapper<LongWritable, Text, Text, MobileDataWritable> {

		public Text outPutKey = new Text();
		public MobileDataWritable outPutValue = new MobileDataWritable();

		@Override
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			System.out.println(key+":"+value);
			String[] arr = value.toString().split("\t");
			outPutKey.set(arr[1]);
			outPutValue.set(Long.valueOf(arr[6]), Long.valueOf(arr[7]),
					Long.valueOf(arr[8]), Long.valueOf(arr[9]));
			context.write(outPutKey, outPutValue);
		}

	}

	// reduce class
	/***
	 * 
	 * @author nele
	 * 
	 */
	public static class MobileDataReducer extends
			Reducer<Text, MobileDataWritable, Text, MobileDataWritable> {

		private Text outPutKey = new Text();
		private MobileDataWritable outPutValue = new MobileDataWritable();

		@Override
		public void reduce(Text key, Iterable<MobileDataWritable> values,
				Context context) throws IOException, InterruptedException {
			long upPackNum = 0;
			long downPackNum = 0;
			long upPayLoad = 0;
			long downPayLoad = 0;
			for (MobileDataWritable val : values) {
				upPackNum += val.getUpPackNum();
				downPackNum += val.getDownPackNum();
				upPayLoad += val.getUpPayLoad();
				downPayLoad += val.getDownPayLoad();
			}
           outPutKey.set(key);
           outPutValue.set(upPackNum, downPackNum, upPayLoad, downPayLoad);
           context.write(outPutKey, outPutValue);
		}
	}

	// run method
	public int run(String[] args) throws Exception {
		Configuration conf = super.getConf();

		// create job
		Job job = Job.getInstance(conf, this.getClass().getSimpleName());
		job.setJarByClass(this.getClass());

		// set input path
		Path inPath = new Path(args[0]);
		FileInputFormat.addInputPath(job, inPath);

		// map
		job.setMapperClass(MobileDataMapper.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(MobileDataWritable.class);

		// conbile
		job.setCombinerClass(MobileDataReducer.class);

		// reduce
		job.setReducerClass(MobileDataReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(MobileDataWritable.class);

		// output
		Path outPath = new Path(args[1]);
		FileOutputFormat.setOutputPath(job, outPath);

		// submit
		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
//		 args = new String[] {
//		 "hdfs://bigdata5:8020/user/nele/data/input/HTTP_20130313143750.data",
//		 "hdfs://bigdata5:8020/user/nele/data/output/output6" };

		Configuration conf = new Configuration();

		int status = ToolRunner.run(conf, new MobileDataMapReduce(), args);

		System.exit(status);
	}

}
