package org.apache.hadoop.mapreduce.app;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/***
 * MapReduce Module
 * 
 * @author nele
 * 
 */
public class WebPvMapReduce extends Configured implements Tool {

	// map class
	/**
	 * 
	 * @author nele
	 * 
	 */
	public static class WebPvMapper extends
			Mapper<LongWritable, Text, IntWritable, LongWritable> {

		public IntWritable outputKey = new IntWritable();
		public LongWritable outputValue = new LongWritable(1);

		@Override
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			String inlineValue = value.toString();
			String[] values = inlineValue.split("\\t");

			if (30 > values.length) {
				context.getCounter("WebPvCounter", "WebPvCounter_LT30").increment(1L);  //add counter
				return;
			}

			String provinceIdValue = values[23];

			String url = values[1];

			if (StringUtils.isBlank(provinceIdValue)) {
				context.getCounter("WebPvCounter", "WebPvCounter_ProvinceIdEmpty").increment(1L);  //add counter
				return;
			}
			if (StringUtils.isBlank(url)) {
				context.getCounter("WebPvCounter", "WebPvCounter_UrlEmpty").increment(1L);  //add counter
				return;
			}

			int provinceId = Integer.MAX_VALUE;

			try {
				provinceId = Integer.valueOf(provinceIdValue);
			} catch (Exception ex) {
				context.getCounter("WebPvCounter", "WebPvCounter_ProvinceIdConvertErr").increment(1L);  //add counter
				return;
			}

			if (Integer.MAX_VALUE == provinceId) {
				context.getCounter("WebPvCounter", "WebPvCounter_ProvinceIdConvertFail").increment(1L);  //add counter
				return;
			}

			outputKey.set(provinceId);

			context.write(outputKey, outputValue);
		}

	}

	// reduce class
	/***
	 * 
	 * @author nele
	 * 
	 */
	public static class WebPvReducer extends
			Reducer<IntWritable, LongWritable, IntWritable, LongWritable> {

		@Override
		public void reduce(IntWritable key, Iterable<LongWritable> values,
				Context context) throws IOException, InterruptedException {
			long sum = 0;
			LongWritable outputValue = new LongWritable();
			for (LongWritable val : values) {
				sum += val.get();
			}
			outputValue.set(sum);
			context.write(key, outputValue);
		}

	}

	// run method
	public int run(String[] args) throws Exception {
		Configuration conf = super.getConf();

		// create job
		Job job = Job.getInstance(conf, this.getClass().getSimpleName());
		job.setJarByClass(this.getClass());

		// set input path
		Path inPath = new Path(args[0]);
		FileInputFormat.addInputPath(job, inPath);

		// map
		job.setMapperClass(WebPvMapper.class);
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(LongWritable.class);

		// ########################shuffle###########################################
		// 1. partitioner
		// job.setPartitionerClass(cls);
		// 2.sort
		// job.setSortComparatorClass(cls);
		// 3. conbine
		// job.setCombinerClass(ModuleReducer.class);
		// 4. compress
		// set by configuraation
		// 5.group
		// job.setGroupingComparatorClass(cls);

		// ########################shuffle###########################################

		// reduce
		job.setReducerClass(WebPvReducer.class);
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(LongWritable.class);

		// output
		Path outPath = new Path(args[1]);
		FileOutputFormat.setOutputPath(job, outPath);

		// submit
		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		args = new String[] {
				"hdfs://bigdata5:8020/user/nele/data/input/2015082818",
				"hdfs://bigdata5:8020/user/nele/data/output/output2" };

		Configuration conf = new Configuration();

		int status = ToolRunner.run(conf, new WebPvMapReduce(), args);

		System.exit(status);
	}

}
