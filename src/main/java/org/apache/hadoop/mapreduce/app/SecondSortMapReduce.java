package org.apache.hadoop.mapreduce.app;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.group.FirstGroupingComparator;
import org.apache.hadoop.mapreduce.io.PairWritable;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.partitioner.FirstPartitioner;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/***
 * SecondSort
 * 
 * @author nele
 * 
 */
public class SecondSortMapReduce extends Configured implements Tool {

	// map class
	/**
	 * 
	 * @author nele
	 * 
	 */
	public static class SecondSortMapper extends
			Mapper<LongWritable, Text, PairWritable, IntWritable> {

		private PairWritable outputKey = new PairWritable();
		private IntWritable outputValue = new IntWritable();

		@Override
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			 String lineValue = value.toString();
			 String[] values = lineValue.split(",");
			 outputKey.set(values[0], Integer.valueOf(values[1]));
			 outputValue.set(Integer.valueOf(values[1]));
			 context.write(outputKey, outputValue);
		}

	}

	// reduce class
	/***
	 * 
	 * @author nele
	 * 
	 */
	public static class SecondSortReducer extends
			Reducer<PairWritable, IntWritable, Text, IntWritable> {

		private Text outputKey  =new Text();
		@Override
		public void reduce(PairWritable key, Iterable<IntWritable> values,
				Context context) throws IOException, InterruptedException {
			 System.out.println(key.getFirst());
			 outputKey.set(key.getFirst());
			 for(IntWritable val : values){
				context.write(outputKey, val); 
			 } 
		}

	}

	// run method
	public int run(String[] args) throws Exception {
		Configuration conf = super.getConf();

		// create job
		Job job = Job.getInstance(conf, this.getClass().getSimpleName());
		job.setJarByClass(this.getClass());

		// set input path
		Path inPath = new Path(args[0]);
		FileInputFormat.addInputPath(job, inPath);

		// map
		job.setMapperClass(SecondSortMapper.class);
		job.setMapOutputKeyClass(PairWritable.class);
		job.setMapOutputValueClass(IntWritable.class);

		// ########################shuffle###########################################
		// 1. partitioner
		job.setPartitionerClass(FirstPartitioner.class);
		// 2.sort
		// job.setSortComparatorClass(cls);
		// 3. conbine
		// job.setCombinerClass(ModuleReducer.class);
		// 4. compress
		// set by configuraation
		// 5.group
	    job.setGroupingComparatorClass(FirstGroupingComparator.class);

		// ########################shuffle###########################################

		// reduce
		job.setReducerClass(SecondSortReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
          //set reducer number
		job.setNumReduceTasks(2);
		
		// output
		Path outPath = new Path(args[1]);
		FileOutputFormat.setOutputPath(job, outPath);

		// submit
		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		 args = new String[] {
		 "hdfs://bigdata5:8020/user/nele/data/input/secondsort.txt",
		 "hdfs://bigdata5:8020/user/nele/data/output/output8" };

		Configuration conf = new Configuration();

		int status = ToolRunner.run(conf, new SecondSortMapReduce(), args);

		System.exit(status);
	}

}
