package org.apache.hadoop.mapreduce.io;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

public class PairWritable implements WritableComparable<PairWritable>{

	
	private String first;
	
	private int second;
	
	
	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}
	
	public PairWritable() {}

	public PairWritable(String first, int second) {
		 set(first,second);
	}

	public void set(String first,int second){
	   this.first = first;
	   this.second = second;
	}
	
	
	public void write(DataOutput out) throws IOException {
		  out.writeUTF(this.first);
		  out.writeInt(second);
	}

	public void readFields(DataInput in) throws IOException {
		 this.first =  in.readUTF();
		 this.second = in.readInt();
	}

	
	public int compareTo(PairWritable o) {
		int comp = this.first.compareTo(o.first);
		if(comp !=0){
			return comp ;
		}else{
			comp = Integer.valueOf(this.second).compareTo(Integer.valueOf(o.second));
			return comp;
		}
	}
	

}
