package org.apache.hadoop.mapreduce.io;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;
 

/***
 *  customize mobile data writable
 * @author nele
 *
 */
public class MobileDataWritable implements
		Writable {

	private long upPackNum;

	private long downPackNum;

	private long upPayLoad;

	private long downPayLoad;

	public long getUpPackNum() {
		return upPackNum;
	}

	public void setUpPackNum(long upPackNum) {
		this.upPackNum = upPackNum;
	}

	public long getDownPackNum() {
		return downPackNum;
	}

	public void setDownPackNum(long downPackNum) {
		this.downPackNum = downPackNum;
	}

	public long getUpPayLoad() {
		return upPayLoad;
	}

	public void setUpPayLoad(long upPayLoad) {
		this.upPayLoad = upPayLoad;
	}

	public long getDownPayLoad() {
		return downPayLoad;
	}

	public void setDownPayLoad(long downPayLoad) {
		this.downPayLoad = downPayLoad;
	}

	public MobileDataWritable() {
	}

	public MobileDataWritable(long upPackNum, long downPackNum, long upPayLoad,
			long downPayLoad) {
		this.set(upPackNum, downPackNum, upPayLoad, downPayLoad);
	}

	public void set(long upPackNum, long downPackNum, long upPayLoad,
			long downPayLoad) {
		this.upPackNum = upPackNum;
		this.downPackNum = downPackNum;
		this.upPayLoad = upPayLoad;
		this.downPayLoad = downPayLoad;
	}

	public void write(DataOutput out) throws IOException {
		out.writeLong(upPackNum);
		out.writeLong(downPackNum);
		out.writeLong(upPayLoad);
		out.writeLong(downPayLoad);
	}

	public void readFields(DataInput in) throws IOException {
		this.upPackNum = in.readLong();
		this.downPackNum = in.readLong();
		this.upPayLoad = in.readLong();
		this.downPayLoad = in.readLong();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (downPackNum ^ (downPackNum >>> 32));
		result = prime * result + (int) (downPayLoad ^ (downPayLoad >>> 32));
		result = prime * result + (int) (upPackNum ^ (upPackNum >>> 32));
		result = prime * result + (int) (upPayLoad ^ (upPayLoad >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobileDataWritable other = (MobileDataWritable) obj;
		if (downPackNum != other.downPackNum)
			return false;
		if (downPayLoad != other.downPayLoad)
			return false;
		if (upPackNum != other.upPackNum)
			return false;
		if (upPayLoad != other.upPayLoad)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return upPackNum + "\t" +downPackNum+ "\t" + upPayLoad + "\t" + downPayLoad;
	}


}
